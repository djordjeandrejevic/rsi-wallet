'use strict';

const bodyParser = require('body-parser'),
express = require('express'),
morgan = require('morgan');

let balance = 123100;

let app = express();

app.use(bodyParser.json());
app.use(morgan('dev'));

app.post('/startSession', (req, res) => {
  console.log(req);
  res.json({
    sessionId: 'i0kXKSLcynaH',
    playerId: 'player123',
    balanceInCents: `${balance}`,
    // code: 'NO_DATA',
    // message: 'Some error!'
  });
});

app.post('/balance', (req, res) => {
  console.log(req);
  res.json({
    balanceInCents: `${balance}`,
    // code: 'NO_DATA',
    // message: 'Some error!'
  });
});

app.post('/bet', (req, res) => {
  console.log(req.body);
  balance = balance - req.query.amountInCents;
  res.json({
    balanceInCents: `${balance}`,
    // code: 'NO_DATA',
    // message: 'Some error!'
  });
});

app.post('/win', (req, res) => {
  console.log(req.body);
  balance = balance + req.query.amountInCents;
  res.json({
    balanceInCents: `${balance}`,
    // code: 'NO_DATA',
    // message: 'Some error!'
  });
});

app.post('/cancelRound', (req, res) => {
  console.log(req.body);
  balance = balance + 2;
  res.json({
    balanceInCents: `${balance}`,
    // code: 'NO_DATA',
    // message: 'Some error!'
  });
});

app.listen(2000, () => {
  console.log('Server up and listening on 2000!');
});
